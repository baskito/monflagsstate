
namespace MonFlagsState.Localization
{
    /// <summary>
    /// Represents localization strings in MonLangLogic.
    /// </summary>
    public class MonFlagsStateId
    {
        public const string ExArgumentType = "ExArgumentType";
        public const string ExKeyNotFound = "ExKeyNotFound";
        public const string ExItemLastState = "ExItemLastState";
        public const string ExItemFirstState = "ExItemFirstState";
    }
}
