﻿using System;
using System.ComponentModel;
using System.Windows.Forms;

namespace MonFlagsState.Example
{
    public partial class Form1 : Form
    {
        public enum EFlags
        {
            FLAG1 = 0,
            FLAG2 = 1,
            FLAG3 = 2,
            FLAG4 = 3
        }

        public enum EStates
        {
            [Description("This is state 1")]
            STATE1 = 0,
            [Description("This is state 2")]
            STATE2 = 1,
            [Description("This is state 3")]
            STATE3 = 2
        }


        public Form1()
        {
            InitializeComponent();
        }

        private void Form1_Load(object sender, EventArgs e)
        {
            MonFlagsState.FlagsState<EFlags, EStates> states = new FlagsState<EFlags, EStates>();

            states.FlagsStateChangeEvent += States_FlagsStateChangeEvent;

            Console.WriteLine("FLAG1 IS IN STATE STATE1?: " + states.IsInState(EFlags.FLAG1, EStates.STATE1));
            Console.WriteLine("FLAG1 IS IN STATE STATE2?: " + states.IsInState(EFlags.FLAG1, EStates.STATE2));

            Console.WriteLine("CHANGE FLAG1 TO STATE2");
            states.SetNewState(EFlags.FLAG1, EStates.STATE2);

            Console.WriteLine("FLAG1 IS IN STATE STATE2?: " + states.IsInState(EFlags.FLAG1, EStates.STATE2));

            Console.WriteLine("CHANGE FLAG1 TO NEXT STATE");            
            states.SetNextState(EFlags.FLAG1);

            Console.WriteLine("FLAG1 IS IN STATE STATE3?: " + states.IsInState(EFlags.FLAG1, EStates.STATE3));

            Console.WriteLine("CHANGE FLAG1 TO NEXT STATE");            
            states.SetNextState(EFlags.FLAG1);

            Console.WriteLine("FLAG1 IS IN STATE STATE1?: " + states.IsInState(EFlags.FLAG1, EStates.STATE1));
            Console.WriteLine("FLAG1 IS IN STATE STATE2?: " + states.IsInState(EFlags.FLAG1, EStates.STATE2));

            states.ExecuteEventAsync = true;

            Console.WriteLine("Get actual state: " + states.GetState(EFlags.FLAG1));

            Console.WriteLine("CHANGE FLAG1 TO PREVIOUS STATE");
            states.SetPreviousState(EFlags.FLAG1);
            Console.WriteLine("Get actual state: " + states.GetState(EFlags.FLAG1));

            Console.WriteLine("CHANGE FLAG1 TO PREVIOUS STATE");
            states.SetPreviousState(EFlags.FLAG1);
            Console.WriteLine("Get actual state: " + states.GetState(EFlags.FLAG1));

            Console.WriteLine("CHANGE FLAG1 TO PREVIOUS STATE");
            states.SetPreviousState(EFlags.FLAG1);
            Console.WriteLine("Get actual state: " + states.GetState(EFlags.FLAG1));
        }

        private void States_FlagsStateChangeEvent(object sender, FlagsStateChangeEventArgs<Form1.EFlags, Form1.EStates> e)
        {
            Console.WriteLine("Wait!!! State {0} changed from {1} to {2}", e.Flag, e.OldState, e.NewState);
        }
    }
}
