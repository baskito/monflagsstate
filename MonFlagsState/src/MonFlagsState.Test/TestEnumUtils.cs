﻿using MonFlagsState.Commons;
using NUnit.Framework;
using System.Collections.Generic;

namespace MonFlagsState.Test
{
    [TestFixture]
    public class TestEnumUtils
    {
        private enum EStateEmpty
        { }

        private enum EState
        {
            [System.ComponentModel.Description("Desc State 1")]
            State1 = 0,

            [System.ComponentModel.Description("Desc State 2")]
            State2 = 1,

            State3
        }

        [Test]
        public void TestEnumUtilsGetFirst()
        {
            Assert.AreEqual(EState.State1, EnumUtils.GetFirst<EState>());
        }

        [Test]
        public void TestEnumUtilsGetLast()
        {
            Assert.AreEqual(EState.State3, EnumUtils.GetLast<EState>());
        }

        [Test]
        public void TestEnumUtilsGetNext()
        {
            EState state = EState.State1;
            Assert.AreEqual(EState.State2, state.Next<EState>());

            state = EState.State2;
            Assert.AreEqual(EState.State3, state.Next<EState>());

            state = EState.State3;
            Assert.AreEqual(EState.State1, state.Next<EState>());
        }

        [Test]
        public void TestEnumUtilsGetPrevious()
        {
            EState state = EState.State1;
            Assert.AreEqual(EState.State3, state.Previous<EState>());
        }

        [Test]
        public void TestEnumUtilsEnumToListEmptyEnum()
        {
            List<EStateEmpty> lstStates = EnumUtils.EnumToList<EStateEmpty>();

            Assert.IsNotNull(lstStates);
            Assert.IsEmpty(lstStates);
        }

        [Test]
        public void TestEnumUtilsEnumToList()
        {
            List<EState> lstStates = EnumUtils.EnumToList<EState>();

            Assert.IsNotNull(lstStates);
            Assert.IsNotEmpty(lstStates);

            Assert.AreEqual(3, lstStates.Count);
        }
    }
}