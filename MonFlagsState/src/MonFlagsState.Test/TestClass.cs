﻿using NUnit.Framework;
using NUnit.Framework.Compatibility;
using System.Threading;

namespace MonFlagsState.Test
{
    [TestFixture]
    public class TestClass
    {
        private enum EFlag
        {
            Flag1
        }

        private enum EState
        {
            State1,
            State2,
            State3
        }

        [Test(Description = "Test if initializes correctly")]
        public void TestFlagInitialize()
        {
            MonFlagsState.FlagsState<EFlag, EState> mon = new MonFlagsState.FlagsState<EFlag, EState>();
            Assert.AreEqual(EState.State1, mon.GetState(EFlag.Flag1));
        }

        [Test(Description = "")]
        public void TestFlag()
        {
            MonFlagsState.FlagsState<EFlag, EState> mon = new MonFlagsState.FlagsState<EFlag, EState>();
            mon.SetNewState(EFlag.Flag1, EState.State3);

            Assert.AreEqual(EState.State3, mon.GetState(EFlag.Flag1));
        }

        [Test]
        public void TestFlagNextState()
        {
            MonFlagsState.FlagsState<EFlag, EState> mon = new MonFlagsState.FlagsState<EFlag, EState>();
            mon.SetNextState(EFlag.Flag1);
            Assert.AreEqual(EState.State2, mon.GetState(EFlag.Flag1));
        }

        [Test]
        public void TestFlagPreviousState()
        {
            MonFlagsState.FlagsState<EFlag, EState> mon = new MonFlagsState.FlagsState<EFlag, EState>();
            mon.SetPreviousState(EFlag.Flag1);
            Assert.AreEqual(EState.State3, mon[EFlag.Flag1]);
        }

        [Test]
        public void TestFlagIsInState()
        {
            MonFlagsState.FlagsState<EFlag, EState> mon = new MonFlagsState.FlagsState<EFlag, EState>();
            mon.SetPreviousState(EFlag.Flag1);
            Assert.True(mon.IsInState(EFlag.Flag1, EState.State3));
        }

        [Test]
        public void TestFlagEventChange()
        {
            AutoResetEvent _autoResetEvent = new AutoResetEvent(false);

            EState actual = EState.State1;
            EState old = EState.State1;

            MonFlagsState.FlagsState<EFlag, EState> mon = new MonFlagsState.FlagsState<EFlag, EState>();
            mon.ExecuteEventAsync = false;

            mon.SetNewState(EFlag.Flag1, EState.State2);

            var sw = new Stopwatch();
            sw.Start();

            mon.FlagsStateChangeEvent += (_, s) =>
            {
                actual = s.NewState;
                old = s.OldState;
                _autoResetEvent.Set();
            };

            mon.SetNextState(EFlag.Flag1);

            sw.Stop();

            Assert.Less(sw.ElapsedMilliseconds, 500);
            Assert.IsTrue(_autoResetEvent.WaitOne());

            Assert.AreEqual(EState.State2, old);
            Assert.AreEqual(EState.State3, actual);
        }
    }
}