﻿using MonFlagsState.Localization;

namespace MonFlagsState
{
    public static class Utils
    {
        /// <summary>
        /// Gets the text assigned to the id.
        /// </summary>
        /// <param name="id">Text identifier.</param>
        /// <returns>Text assigned to the id</returns>
        public static string GetLocalString(string id)
        {
            return MonFlagsStateLocalizationProvider.CurrentProvider.GetLocalizedString(id);
        }
    }
}