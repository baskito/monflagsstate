﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;

namespace MonFlagsState.Commons
{
    /// <summary>
    /// Class with useful functions for handling Enums.
    /// </summary>
    public static class EnumUtils
    {
        /// <summary>
        /// Returns a list of elements of the enum type.
        /// </summary>
        /// <typeparam name="T">Type of enumerator.</typeparam>
        /// <returns>List with enum values.</returns>
        public static List<T> EnumToList<T>()
        {
            return Enum.GetValues(typeof(T)).Cast<T>().ToList();
        }

        /// <summary>
        /// Gets the first value of an enumerator.
        /// </summary>
        /// <typeparam name="T">Type of enumerator.</typeparam>
        /// <returns>First value of enumerator.</returns>
        public static T GetFirst<T>() where T : struct
        {
            if (!typeof(T).IsEnum) throw new ArgumentException(String.Format("Argumnent {0} is not an Enum", typeof(T).FullName));

            Enum.GetValues(typeof(T)).Cast<T>().Last();

            return Enum.GetValues(typeof(T)).Cast<T>().First();
        }

        /// <summary>
        /// Gets the last value of an enumerator.
        /// </summary>
        /// <typeparam name="T">Type of enumerator.</typeparam>
        /// <returns>Last value of enumerator.</returns>
        public static T GetLast<T>() where T : struct
        {
            if (!typeof(T).IsEnum) throw new ArgumentException(String.Format("Argumnent {0} is not an Enum", typeof(T).FullName));

            return Enum.GetValues(typeof(T)).Cast<T>().Last();
        }

        /// <summary>
        /// Gets the next value of an enumerator.
        /// </summary>
        /// <typeparam name="T">Type of enumerator.</typeparam>
        /// <param name="src">Initial value from which the next value will be obtained.</param>
        /// <returns>Next value.</returns>
        /// <remarks>If it is the last state, is selected the first state of the enumerator.</remarks>
        public static T Next<T>(this T src) where T : struct
        {
            if (!typeof(T).IsEnum) throw new ArgumentException(String.Format("Argumnent {0} is not an Enum", typeof(T).FullName));

            T[] Arr = (T[])Enum.GetValues(src.GetType());
            int j = Array.IndexOf<T>(Arr, src) + 1;
            return (Arr.Length == j) ? Arr[0] : Arr[j];
        }

        /// <summary>
        /// Gets the previous value of an enumerator.
        /// </summary>
        /// <typeparam name="T">Type of enumerator.</typeparam>
        /// <param name="src">Initial value from which the previous value will be obtained.</param>
        /// <returns>Previous value.</returns>
        /// <remarks>If it is the first state, is selected the last state of the enumerator.</remarks>
        public static T Previous<T>(this T src) where T : struct
        {
            if (!typeof(T).IsEnum) throw new ArgumentException(String.Format("Argumnent {0} is not an Enum", typeof(T).FullName));

            T[] Arr = (T[])Enum.GetValues(src.GetType());
            int j = Array.IndexOf<T>(Arr, src) - 1;
            return (j < 0) ? Arr[Arr.Length - 1] : Arr[j];
        }
    }
}