﻿using MonFlagsState.Commons;
using MonFlagsState.Localization;
using System;
using System.Collections.Generic;

namespace MonFlagsState
{
    public class FlagsState<T, E>
        where T : struct, IConvertible
        where E : struct, IConvertible
    {
        #region -[Events]-

        /// <summary>
        /// Event generated when the state of a flag is modified.
        /// </summary>
        public event EventHandler<FlagsStateChangeEventArgs<T, E>> FlagsStateChangeEvent;

        #endregion -[Events]-

        #region -[Private variables]-

        /// <summary>
        /// Dictionary to store the flag and its status.
        /// </summary>
        private Dictionary<T, E> _dicFlagsStates = null;

        private bool _moveNextThrowErrorIfLast = false;

        private bool _movePreviousThrowErrorIfFirst = false;

        #endregion -[Private variables]-

        #region -[Constructor]-

        public FlagsState()
        {
            if (!typeof(T).IsEnum)
            {
                throw new ArgumentException(String.Format(Utils.GetLocalString(MonFlagsStateId.ExArgumentType), "T"));
            }

            if (!typeof(E).IsEnum)
            {
                throw new ArgumentException(String.Format(Utils.GetLocalString(MonFlagsStateId.ExArgumentType), "E"));
            }

            this.ExecuteEventAsync = false;

            CreateDictionary();
        }

        #endregion -[Constructor]-

        #region -[Private methods]-

        private void CreateDictionary()
        {
            _dicFlagsStates = new Dictionary<T, E>();

            E first = EnumUtils.GetFirst<E>();

            foreach (var e in EnumUtils.EnumToList<T>())
            {
                _dicFlagsStates.Add(e, first);
            }
        }

        private void OnFlagStateChange(FlagsStateChangeEventArgs<T, E> e)
        {
            EventHandler<FlagsStateChangeEventArgs<T, E>> handle = FlagsStateChangeEvent;

            if (handle != null)
            {
                handle(this, e);
            }
        }

        private void OnFlagStateChangeAsync(FlagsStateChangeEventArgs<T, E> e)
        {
            EventHandler<FlagsStateChangeEventArgs<T, E>> handle = FlagsStateChangeEvent;

            if (handle != null)
            {
                var eventListeners = handle.GetInvocationList();

                for (int index = 0; index < eventListeners.Length; index++)
                {
                    var methodToInvoke = (EventHandler<FlagsStateChangeEventArgs<T, E>>)eventListeners[index];
                    methodToInvoke.BeginInvoke(null, e, null, null);
                }
            }
        }

        #endregion -[Private methods]-

        #region -[Public properties]-

        /// <summary>
        /// Flag that stores if the event is executed asynchronously.
        /// </summary>
        public bool ExecuteEventAsync
        {
            get;
            set;
        }

        /// <summary>
        /// Get the dictionary with all the flags and their current states.
        /// </summary>
        public Dictionary<T, E> FlagsStates
        {
            get { return _dicFlagsStates; }
        }

        public bool MoveNextThrowErrorIfLast
        {
            get
            {
                return _moveNextThrowErrorIfLast;
            }

            set
            {
                _moveNextThrowErrorIfLast = value;
            }
        }

        public bool MovePreviousThrowErrorIfFirst
        {
            get
            {
                return _movePreviousThrowErrorIfFirst;
            }

            set
            {
                _movePreviousThrowErrorIfFirst = value;
            }
        }

        /// <summary>
        /// The Item property provides methods for reading states of flags in the dictionary.
        /// </summary>
        /// <param name="flag">Flag to check.</param>
        /// <returns>Current state of the flag</returns>
        public E this[T flag]
        {
            get { return _dicFlagsStates[flag]; }
        }
        #endregion -[Public properties]-

        #region -[Public methods]-

        /// <summary>
        /// Gets state of flags.
        /// </summary>
        /// <param name="flag">Flag to check.</param>
        /// <returns>Current state of the flag,</returns>
        /// <remarks>If it is the last state, is selected the first state of the enumerator.</remarks>
        public E GetState(T flag)
        {
            if (!_dicFlagsStates.ContainsKey(flag)) throw new KeyNotFoundException(String.Format(Utils.GetLocalString(MonFlagsStateId.ExKeyNotFound), flag.ToString()));

            return _dicFlagsStates[flag];
        }

        /// <summary>
        /// Checks if flag is in a state.
        /// </summary>
        /// <param name="flag">Flag to check.</param>
        /// <param name="state"></param>
        /// <returns>True if the indicator is in the indicated state. False if not.</returns>
        /// <remarks>If it is the last state, is selected the first state of the enumerator.</remarks>
        public bool IsInState(T flag, E state)
        {
            if (!_dicFlagsStates.ContainsKey(flag)) throw new KeyNotFoundException(String.Format(Utils.GetLocalString(MonFlagsStateId.ExKeyNotFound), flag.ToString()));

            return _dicFlagsStates[flag].Equals(state);
        }
        /// <summary>
        /// Restore all flags to their first state.
        /// </summary>
        public void Restart()
        {
            CreateDictionary();
        }

        /// <summary>
        /// Restore a flag to its first state.
        /// </summary>
        /// <param name="flag">Flag to restore.</param>
        /// <exception cref="KeyNotFoundException">Flag not found in the dictionary.</exception>
        public void Restart(T flag)
        {
            if (!_dicFlagsStates.ContainsKey(flag)) throw new KeyNotFoundException(String.Format(Utils.GetLocalString(MonFlagsStateId.ExKeyNotFound), flag.ToString()));

            _dicFlagsStates[flag] = EnumUtils.GetFirst<E>();
        }

        /// <summary>
        /// Defines the state of the flag.
        /// </summary>
        /// <param name="flag">Flag to change state.</param>
        /// <param name="state">New state.</param>
        /// <remarks>If it is the last state, is selected the first state of the enumerator.</remarks>
        public void SetNewState(T flag, E state)
        {
            if (!_dicFlagsStates.ContainsKey(flag)) throw new KeyNotFoundException(String.Format(Utils.GetLocalString(MonFlagsStateId.ExKeyNotFound), flag.ToString()));

            E oldState = _dicFlagsStates[flag];
            E newState = state;

            _dicFlagsStates[flag] = newState;

            if (!newState.Equals(oldState))
            {
                if (ExecuteEventAsync)
                    OnFlagStateChangeAsync(new FlagsStateChangeEventArgs<T, E>(flag, newState, oldState));
                else
                    OnFlagStateChange(new FlagsStateChangeEventArgs<T, E>(flag, newState, oldState));
            }
        }

        /// <summary>
        /// Defines the state in the next order of the flag.
        /// </summary>
        /// <param name="flag">Flag to change state.</param>
        /// <exception cref="KeyNotFoundException">Flag not found in the dictionary.</exception>
        /// <remarks>If it is the last state, is selected the first state of the enumerator.</remarks>
        public void SetNextState(T flag)
        {
            if (!_dicFlagsStates.ContainsKey(flag)) throw new KeyNotFoundException(String.Format(Utils.GetLocalString(MonFlagsStateId.ExKeyNotFound), flag.ToString()));

            if (MoveNextThrowErrorIfLast)
            {
                if (GetState(flag).Equals(EnumUtils.GetLast<E>()))
                {
                    throw new Exception(String.Format(Utils.GetLocalString(MonFlagsStateId.ExItemLastState), flag.ToString()));
                }
            }

            E localState = _dicFlagsStates[flag].Next<E>();

            SetNewState(flag, localState);
        }

        /// <summary>
        /// Defines the state in the previous order of the flag.
        /// </summary>
        /// <param name="flag">Flag to change state.</param>
        /// <exception cref="KeyNotFoundException">Flag not found in the dictionary.</exception>
        /// <remarks>If it is the first state, is selected the last state of the enumerator.</remarks>
        public void SetPreviousState(T flag)
        {
            if (!_dicFlagsStates.ContainsKey(flag)) throw new KeyNotFoundException(String.Format(Utils.GetLocalString(MonFlagsStateId.ExKeyNotFound), flag.ToString()));

            if (MovePreviousThrowErrorIfFirst)
            {
                if (GetState(flag).Equals(EnumUtils.GetFirst<E>()))
                {
                    throw new Exception(String.Format(Utils.GetLocalString(MonFlagsStateId.ExItemFirstState), flag.ToString()));
                }
            }

            E localState = _dicFlagsStates[flag].Previous<E>();

            SetNewState(flag, localState);
        }
        #endregion -[Public methods]-
    }
}