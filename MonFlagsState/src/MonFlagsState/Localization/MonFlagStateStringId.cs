namespace MonFlagsState.Localization
{
    /// <summary>
    /// Represents localization strings in MonLangLogic.
    /// </summary>
    public class MonFlagsStateId
    {
        public const string ExArgumentType = "ExArgumentType";
        public const string ExItemFirstState = "ExItemFirstState";
        public const string ExItemLastState = "ExItemLastState";
        public const string ExKeyNotFound = "ExKeyNotFound";
    }
}