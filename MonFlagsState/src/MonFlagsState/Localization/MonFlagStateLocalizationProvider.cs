﻿namespace MonFlagsState.Localization
{
    /// <summary>
    /// Provides localization services for MonLangLogic
    /// </summary>
    public class MonFlagsStateLocalizationProvider : LocalizationProvider<MonFlagsStateLocalizationProvider>
    {
        public override string GetLocalizedString(string id)
        {
            switch (id)
            {
                case MonFlagsStateId.ExArgumentType: return "{0} must be an enumerated type";
                case MonFlagsStateId.ExKeyNotFound: return "Enum '{0}' not found in the enumeration";
                case MonFlagsStateId.ExItemFirstState: return "The item {0} is in the first state.";
                case MonFlagsStateId.ExItemLastState: return "The item {0} is in the last state.";
            }

            return string.Empty;
        }
    }
}