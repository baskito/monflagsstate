﻿using System;

namespace MonFlagsState
{
    public class FlagsStateChangeEventArgs<T, E> : EventArgs
    {
        private readonly T _flag;

        private readonly E _newState;

        private readonly E _oldState;

        public FlagsStateChangeEventArgs(T flag, E newState, E oldState)
        {
            _flag = flag;
            _newState = newState;
            _oldState = oldState;
        }

        public T Flag
        {
            get { return _flag; }
        }

        public E NewState
        {
            get { return _newState; }
        }

        public E OldState
        {
            get { return _oldState; }
        }
    }
}